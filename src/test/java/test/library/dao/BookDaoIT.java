package test.library.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import test.library.entity.Book;
import test.library.exception.DaoLayerException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@JdbcTest
@Import(BookDao.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class BookDaoIT {
    @Autowired
    private BookDao bookDao;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Book book;

    @BeforeEach
    public void initData() {
        jdbcTemplate.execute("TRUNCATE TABLE book RESTART IDENTITY");
        jdbcTemplate.update("INSERT INTO book (book_name, author)" +
                            "VALUES ('name1', 'author1')," +
                            "       ('name2', 'author2')," +
                            "       ('name3', 'author3')");
        jdbcTemplate.update("INSERT INTO reader (reader_name) VALUES ('reader1'), ('reader2')");
        book = new Book();
        book.setAuthor("Some author");
        book.setName("Book name");
        book.setId(100L);
    }

    @Test
    public void createShouldReturnGeneratedKey() {
        book.setId(null);
        Book newBook = bookDao.saveNewBook(book);
        assertTrue(newBook.getId() > 0);
        assertEquals(newBook.getName(), book.getName());
        assertEquals(newBook.getAuthor(), book.getAuthor());
    }

    @Test
    public void findAllShouldReturnCorrectAmountOfBooks() {
        assertEquals(3, bookDao.findAllBooks().size());
    }

    @Test
    public void borrowExistAndFreeBookSetReaderId() {
        var readerId = 1L;
        var bookId = 1L;
        bookDao.setBookAsBorrowedBySpecificReader(bookId, readerId);
        Book borrowed = bookDao.findBookByItsId(bookId).get();
        assertEquals(readerId, borrowed.getReaderId());
    }

    @Test
    public void borrowInUseBookThrowDaoLayerException() {
        long bookId = 2L;
        long readerId = 2L;
        jdbcTemplate.update("UPDATE book SET readerId = 1 WHERE id = ?", bookId);
        assertThrows(DaoLayerException.class, () -> bookDao.setBookAsBorrowedBySpecificReader(bookId, readerId));
    }

    @Test
    public void findByWrongIdThrowDaoLayerException() {
        assertThrows(DaoLayerException.class, () -> bookDao.findBookByItsId(125L));
    }

    @Test
    public void returnedBookShouldSetForeignKeyAsNull() {
        jdbcTemplate.update("UPDATE book SET readerId = 1 WHERE id = 1");
        Book inUse = bookDao.findBookByItsId(1L).get();
        assertTrue(inUse.getReaderId() != 0);
        bookDao.setBookAsReturnedToLibrary(1L);
        Book free = bookDao.findBookByItsId(1L).get();
        assertEquals(0, free.getReaderId());
    }

    @Test
    public void returnNotExistsBookThrowException() {
        assertThrows(DaoLayerException.class, () -> bookDao.setBookAsReturnedToLibrary(4L));
    }

    @Test
    public void listBookBorrowedByReaderReturnActualOne() {
        var readerId = 1L;
        jdbcTemplate.update("update book set readerId = 1 where id=1");
        jdbcTemplate.update("update book set readerId = 1 where id=2");
        jdbcTemplate.update("update book set readerId = 2 where id=3");
        var booksByUser = bookDao.findAllBooksByReaderId(readerId);
        var allBooks = bookDao.findAllBooks();
        assertEquals(2, booksByUser.size());
        assertEquals(3, allBooks.size());
        var bookIds = booksByUser.stream()
                .map(Book::getId)
                .sorted()
                .toList();
        assertEquals(List.of(1L, 2L), bookIds);
    }

    @Test
    public void bookListBorrowedByNotExistedUserReturnEmptyList() {
        List<Book> booksByUser = bookDao.findAllBooksByReaderId(100L);
        assertEquals(0, booksByUser.size());
    }

    @Test
    public void mapperShouldWorkCorrect() {
        Book firstBookFromDb = bookDao.findBookByItsId(1L).get();
        assertEquals("name1", firstBookFromDb.getName());
        assertEquals("author1", firstBookFromDb.getAuthor());
        assertEquals(1L, firstBookFromDb.getId());
    }

}
