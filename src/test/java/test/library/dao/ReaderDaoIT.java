package test.library.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import test.library.entity.Reader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@JdbcTest
@Import(ReaderDao.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ReaderDaoIT {
    @Autowired
    private ReaderDao readerDao;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Reader reader;

    @BeforeEach
    public void initData() {
        jdbcTemplate.execute("SET REFERENTIAL_INTEGRITY FALSE");
        jdbcTemplate.execute("TRUNCATE TABLE reader RESTART IDENTITY");
        jdbcTemplate.execute("TRUNCATE TABLE book RESTART IDENTITY");
        jdbcTemplate.execute("SET REFERENTIAL_INTEGRITY TRUE");
        jdbcTemplate.update("INSERT INTO reader (reader_name)" +
                            "VALUES ('reader1')," +
                            "       ('reader2')," +
                            "       ('reader3')");
        reader = new Reader();
        reader.setName("Book name");
        reader.setId(100L);
    }

    @Test
    public void createReturnSameWithGeneratedId() {
        reader.setId(null);
        Reader newReader = readerDao.saveNewReader(reader);
        assertTrue(newReader.getId() > 0);
        assertEquals(reader.getName(), newReader.getName());
    }

    @Test
    public void findAllShouldReturnCorrectAmount() {
        assertEquals(3, readerDao.findAllReaders().size());
    }

    @Test
    public void findCurrentReaderByBookIdWorksCorrect() {
        jdbcTemplate.execute("INSERT INTO book (book_name, author, readerId) " +
                             "VALUES ('bookname1', 'unknown author', 2)");
        Reader readerByBookId = readerDao.findReaderByBookId(1L);
        assertEquals("reader2", readerByBookId.getName());
        assertEquals(2L, readerByBookId.getId());
    }

}
