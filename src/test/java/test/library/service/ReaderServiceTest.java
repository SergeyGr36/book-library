package test.library.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import test.library.dao.ReaderDao;
import test.library.entity.Book;
import test.library.entity.Reader;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ReaderServiceTest {
    @Mock
    private ReaderDao readerDao;
    @InjectMocks
    private ReaderService readerService;
    private Reader reader;
    private Reader expectedReader;

    @BeforeEach
    public void initContext() {
        reader = new Reader();
        reader.setId(100L);
        reader.setName("book_name");
        expectedReader = reader;
        expectedReader.setId(1L);
    }

    @Test
    public void createReaderReturnSameWithGeneratedId() {
        reader.setId(null);
        when(readerDao.saveNewReader(any(Reader.class))).thenReturn(expectedReader);
        var createdReader = readerService.createNewReader(reader);
        assertEquals(expectedReader, createdReader);
    }

    @Test
    public void showAllReadersShouldReturnCorrectObjectsAmount() {
        var createdList = List.of(expectedReader, expectedReader, expectedReader);
        when(readerDao.findAllReaders()).thenReturn(createdList);
        var gotList = readerService.findAllReaders();
        assertEquals(createdList.size(), gotList.size());
        assertTrue(gotList.containsAll(createdList));
    }

    @Test
    public void showOneByBookIdReturnCorrectOne() {
        var book = new Book();
        book.setReaderId(1L);
        when(readerDao.findReaderByBookId(anyLong())).thenReturn(expectedReader);
        Reader returnedReader = readerService.findReaderByBookId(book.getReaderId());
        assertEquals(1L, returnedReader.getId());
        assertEquals(expectedReader, returnedReader);
    }

}
