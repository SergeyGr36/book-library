package test.library.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import test.library.dao.BookDao;
import test.library.entity.Book;
import test.library.entity.Reader;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {
    @Mock
    private BookDao bookDao;
    @InjectMocks
    private BookService bookService;
    private Book book;
    private Book expectedBook;

    @BeforeEach
    public void initContext() {
        book = new Book();
        book.setId(100L);
        book.setName("book_name");
        book.setAuthor("author");
        expectedBook = book;
        expectedBook.setId(1L);
    }

    @Test
    public void createBookReturnSameWithGeneratedId() {
        book.setId(null);
        when(bookDao.saveNewBook(any(Book.class))).thenReturn(expectedBook);
        var createdBook = bookService.createNewBook(book);
        assertEquals(expectedBook, createdBook);
    }

    @Test
    public void showAllBooksShouldReturnCorrectObjectsAmount() {
        var createdList = List.of(expectedBook, expectedBook, expectedBook);
        when(bookDao.findAllBooks()).thenReturn(createdList);
        var gotList = bookService.findAllBooks();
        assertEquals(createdList.size(), gotList.size());
        assertTrue(gotList.containsAll(createdList));
    }

    @Test
    public void showAllBooksBorrowedByReaderReturnCorrectObjectsAmount() {
        var reader = new Reader();
        reader.setId(1L);
        book.setReaderId(1L);
        var expectedBooks = List.of(expectedBook, expectedBook, expectedBook);
        when(bookDao.findAllBooksByReaderId(1L)).thenReturn(expectedBooks);
        var booksByReader = bookService.findAllBooksByReaderId(reader.getId());
        assertEquals(3, booksByReader.size());
        assertTrue((expectedBooks).containsAll(booksByReader));
    }

    @Test
    public void borrowBookCalledValueCaptured() {
        ArgumentCaptor<Long> bookId = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<Long> readerId = ArgumentCaptor.forClass(Long.class);
        doNothing().when(bookDao).setBookAsBorrowedBySpecificReader(bookId.capture(), readerId.capture());
        bookService.borrowBookBySpecificReader(1L, 1L);
        assertEquals(1L, bookId.getValue());
        assertEquals(1L, readerId.getValue());
    }

    @Test
    public void returnBookCalledValueCaptured() {
        ArgumentCaptor<Long> bookId = ArgumentCaptor.forClass(Long.class);
        doNothing().when(bookDao).setBookAsReturnedToLibrary(bookId.capture());
        bookService.returnBookToLibrary(1L);
        assertEquals(1L, bookId.getValue());
    }

}
