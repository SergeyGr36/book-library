INSERT INTO reader (reader_name)
VALUES ('Marilyn Manson'),
       ('Ozzy Osbourne'),
       ('Chester Bennington'),
       ('Kurt Cobain'),
       ('David Draiman') ON CONFLICT DO NOTHING;
INSERT INTO book (book_name, author, readerId)
VALUES ('How to cook a bat', 'Unknown', 2),
       ('Holly Bible', 'Jesus', 1),
       ('Where to buy a rope', 'Alan Smith', 3),
       ('Gun instruction', '27 years old club', 4),
       ('Disturbed or Device', 'Metal band', NULL) ON CONFLICT DO NOTHING;