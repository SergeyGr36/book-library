CREATE TABLE IF NOT EXISTS reader
(
    id          serial PRIMARY KEY,
    reader_name VARCHAR(100)
);
CREATE TABLE IF NOT EXISTS book
(
    id          serial PRIMARY KEY,
    book_name   VARCHAR(100),
    author      VARCHAR(100),
    readerId bigint REFERENCES reader ON DELETE CASCADE
);
