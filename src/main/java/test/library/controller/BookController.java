package test.library.controller;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import test.library.entity.Book;
import test.library.entity.Reader;
import test.library.service.BookService;
import test.library.service.ReaderService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {
    private final BookService bookService;
    private final ReaderService readerService;

    public BookController(BookService bookService, ReaderService readerService) {
        this.bookService = bookService;
        this.readerService = readerService;
    }

    @GetMapping
    public ResponseEntity<List<Book>> getAll() {
        return ResponseEntity.ok(bookService.findAllBooks());
    }


    @GetMapping("/{bookId}/readers")
    public ResponseEntity<Reader> getOneReaderByBookId(@PathVariable Long bookId) {
        return ResponseEntity.ok(readerService.findReaderByBookId(bookId));
    }

    @PostMapping
    public ResponseEntity<Book> createNewBook(@Valid @RequestBody Book book) {
        var newBook = bookService.createNewBook(book);
        return ResponseEntity.ok(newBook);
    }

    @PutMapping("/{bookId}")
    public ResponseEntity<?> updateExistingBook(@Valid @RequestBody Book book, @PathVariable Long bookId) {
        if (book.getReaderId() == null) {
            bookService.returnBookToLibrary(bookId);
        } else {
            bookService.borrowBookBySpecificReader(bookId, book.getReaderId());
        }
        return ResponseEntity.ok().build();
    }

}
