package test.library.controller;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import test.library.entity.Book;
import test.library.entity.Reader;
import test.library.service.BookService;
import test.library.service.ReaderService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/readers")
public class ReaderController {
    private final ReaderService readerService;
    private final BookService bookService;

    public ReaderController(ReaderService readerService, BookService bookService) {
        this.readerService = readerService;
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<List<Reader>> getAll() {
        return ResponseEntity.ok(readerService.findAllReaders());
    }

    @GetMapping("/{readerId}/books")
    public ResponseEntity<List<Book>> getAllBooksByReaderId(@PathVariable Long readerId) {
        return ResponseEntity.ok(bookService.findAllBooksByReaderId(readerId));
    }

    @PostMapping
    public ResponseEntity<Reader> createNewReader(@Valid @RequestBody Reader reader) {
        return ResponseEntity.ok(readerService.createNewReader(reader));
    }

}
