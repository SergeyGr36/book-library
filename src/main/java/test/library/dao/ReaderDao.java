package test.library.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import test.library.entity.Reader;
import test.library.exception.DaoLayerException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Repository
public class ReaderDao {
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    public ReaderDao(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    public Reader saveNewReader(Reader reader) {
        var keyHolder = new GeneratedKeyHolder();
        try {
            namedJdbcTemplate.update("INSERT INTO reader (reader_name) VALUES (:reader_name)",
                    new MapSqlParameterSource("reader_name", reader.getName()), keyHolder);
            var generatedId = Optional.ofNullable(keyHolder.getKey())
                    .map(Number::longValue)
                    .orElseThrow(() -> new DaoLayerException(
                            "Failed to save new Reader to DB, no generated ID returned."));
            reader.setId(generatedId);
            return reader;
        } catch (DataAccessException e) {
            var errorMessage = String.format("Failed to save new reader [%s] to the DB", reader);
            throw new DaoLayerException(errorMessage, e);
        }
    }

    public List<Reader> findAllReaders() {
        try {
            return namedJdbcTemplate.query(
                    "SELECT * FROM reader", this::mapRow);
        } catch (DataAccessException e) {
            throw new DaoLayerException("Failed to fetch all readers from DB: " + e.getLocalizedMessage());
        }
    }

    public Reader findReaderByBookId(Long bookId) {
        try {
            return namedJdbcTemplate.queryForObject(
                    "SELECT r.id, r.reader_name FROM reader r JOIN book b ON b.readerId=r.id WHERE b.id=:bookId",
                    Collections.singletonMap("bookId", bookId),
                    this::mapRow);
        } catch (DataAccessException e) {
            var message = String.format("No one read book with id - %d", bookId);
            throw new DaoLayerException(message);
        }
    }

    private Reader mapRow(ResultSet rs, int rowNum) throws SQLException {
        var reader = new Reader();
        reader.setId(rs.getLong("id"));
        reader.setName(rs.getString("reader_name"));
        return reader;
    }

}
