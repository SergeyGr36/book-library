package test.library.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import test.library.entity.Book;
import test.library.exception.DaoLayerException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class BookDao {
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    public BookDao(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    public Book saveNewBook(Book book) {
        var source = new MapSqlParameterSource()
                .addValue("book_name", book.getName())
                .addValue("author", book.getAuthor());
        var keyHolder = new GeneratedKeyHolder();
        try {
            namedJdbcTemplate.update(
                    "INSERT INTO book (book_name, author) VALUES (:book_name, :author)",
                    source, keyHolder, new String[]{"id"});
            var generatedId = Optional.ofNullable(keyHolder.getKey())
                    .map(Number::longValue)
                    .orElseThrow(() -> new DaoLayerException(
                            "Failed to save new Book to DB, no generated ID returned."));
            book.setId(generatedId);
            return book;
        } catch (DataAccessException e) {
            var errorMessage = String.format("Failed to save new book [%s] to the DB", book);
            throw new DaoLayerException(errorMessage, e);
        }
    }

    public List<Book> findAllBooks() {
        try {
            return namedJdbcTemplate.query(
                    "SELECT * FROM book", this::mapRow);
        } catch (DataAccessException e) {
            throw new DaoLayerException("Failed to execute query by database access exception", e);
        }
    }

    public void setBookAsBorrowedBySpecificReader(Long bookId, Long readerId) {
        int result;
        try {
            result = namedJdbcTemplate.update(
                    "UPDATE book SET readerId = :readerId WHERE id = :id AND readerId IS NULL",
                    Map.of("readerId", readerId,
                            "id", bookId));
            if (result != 1) {
                String exceptionMessage = String.format("Can`t borrow book with 'id' - %d by reader with 'id' - %d",
                        bookId, readerId);
                throw new DaoLayerException(exceptionMessage);
            }
        } catch (DataAccessException e) {
            throw new DaoLayerException("Failed to execute query by database access exception", e);
        }
    }

    public Optional<Book> findBookByItsId(Long bookId) {
        Book book;
        try {
            book = namedJdbcTemplate.queryForObject(
                    "SELECT * FROM book WHERE id = :id",
                    Collections.singletonMap("id", bookId), this::mapRow);
            return Optional.ofNullable(book);
        } catch (DataAccessException e) {
            var errorMessage = String.format("Can`t find book with id - %d", bookId);
            throw new DaoLayerException(errorMessage, e);
        }
    }

    public void setBookAsReturnedToLibrary(Long bookId) {
        int result;
        try {
            result = namedJdbcTemplate.update(
                    "UPDATE book SET readerId = NULL WHERE id = :id",
                    Collections.singletonMap("id", bookId));
            if (result != 1) {
                String exceptionMessage = String.format("Failed to update book with id - '%d' with readerId=NULL:" +
                                                        " SQL request updated 0 rows in [book] DB table.",
                        bookId);
                throw new DaoLayerException(exceptionMessage);
            }
        } catch (DataAccessException e) {
            var errorMessage = String.format("Can`t return book  with id - %d to library", bookId);
            throw new DaoLayerException(errorMessage, e);
        }
    }

    public List<Book> findAllBooksByReaderId(Long readerId) {
        try {
            return namedJdbcTemplate.query(
                    "SELECT * FROM book WHERE readerId = :readerId",
                    Collections.singletonMap("readerId", readerId), this::mapRow);
        } catch (DataAccessException e) {
            throw new DaoLayerException("Failed to execute query by database access exception", e);
        }
    }

    private Book mapRow(ResultSet rs, int rowNum) throws SQLException {
        var book = new Book();
        book.setId(rs.getLong("id"));
        book.setName(rs.getString("book_name"));
        book.setAuthor(rs.getString("author"));
        book.setReaderId(rs.getLong("readerId"));
        return book;
    }

}
