package test.library.exception;

import org.springframework.dao.DataAccessException;

public class DaoLayerException extends DataAccessException {

    public DaoLayerException(String msg) {
        super(msg);
    }

    public DaoLayerException(String message, DataAccessException e) {
        super(message);
        this.setStackTrace(e.getStackTrace());
    }

}
