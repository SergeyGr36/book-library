package test.library.service;

import org.springframework.stereotype.Service;
import test.library.dao.ReaderDao;
import test.library.entity.Reader;

import java.util.List;

@Service
public class ReaderService {
    private final ReaderDao readerDao;

    public ReaderService(ReaderDao readerDao) {
        this.readerDao = readerDao;
    }

    public Reader createNewReader(Reader reader) {
        return readerDao.saveNewReader(reader);
    }

    public List<Reader> findAllReaders() {
        return readerDao.findAllReaders();
    }

    public Reader findReaderByBookId(Long bookId) {
        return readerDao.findReaderByBookId(bookId);
    }

}
