package test.library.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import test.library.dao.BookDao;
import test.library.entity.Book;

import java.util.List;

@Service
public class BookService {
    private static final Logger logger = LoggerFactory.getLogger(BookService.class);
    private final BookDao bookDao;

    public BookService(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public void borrowBookBySpecificReader(Long bookId, Long readerId) {
        bookDao.setBookAsBorrowedBySpecificReader(bookId, readerId);
        String success = String.format("Book with %d id, have been successfully borrowed by reader with id -'%d'",
                bookId, readerId);
        logger.info(success);
    }

    public Book createNewBook(Book book) {
        return bookDao.saveNewBook(book);
    }

    public void returnBookToLibrary(Long bookId) {
        String success = String.format("Book with %d id have been successfully returned", bookId);
        bookDao.setBookAsReturnedToLibrary(bookId);
        logger.info(success);
    }

    public List<Book> findAllBooks() {
        return bookDao.findAllBooks();
    }

    public List<Book> findAllBooksByReaderId(Long readerId) {
        return bookDao.findAllBooksByReaderId(readerId);
    }

}
