package test.library.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import test.library.exception.DaoLayerException;
import test.library.response.ErrorResponse;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class BookLibraryGlobalExceptionResolver {
    private static final Logger logger = LoggerFactory.getLogger(BookLibraryGlobalExceptionResolver.class);

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> validFieldsException(MethodArgumentNotValidException e) {
        List<ErrorResponse.Error> listErrors = new ArrayList<>();
        e.getBindingResult()
                .getFieldErrors()
                .forEach(fieldError -> {
                    var error = new ErrorResponse.Error();
                    error.setFieldName(fieldError.getField());
                    error.setInvalidValue((String) fieldError.getRejectedValue());
                    error.setDetails(fieldError.getDefaultMessage());
                    listErrors.add(error);
                });
        var error = new ErrorResponse();
        error.setErrorMessage("Please populate every field you need");
        error.setDateTime();
        error.setErrors(listErrors);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(DaoLayerException.class)
    public ResponseEntity<String> daoLayerException(DaoLayerException e) {
        logger.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Ooops, something was wrong. Try a little bit latter");
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> noOneKnowsWhatWasWrongException(Exception e) {
        logger.error(String.format("No one knows what was wrong. Look at this error %s", e.getMessage()));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Ooops, something was wrong. Try a little bit latter");
    }

}
